﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterEnemy : Enemy
{
    [SerializeField] float mProjectileSpeed;
    [SerializeField] string mProjectileName;
    [SerializeField] Sprite mWithHatScript;
    [SerializeField] Sprite mWithoutHatSprite;
    void OnEnable()
    {
        if(mSprite == null)
        {
            return;
        }
        mSprite.sprite = mWithHatScript;
    }

    protected override void DoSpecialAbility(Collider2D collision)
    {
        GameObject aProjectileEnemy = ObjectPoolingManager.Instance.GetPooledObject(mProjectileName);
        Enemy aEnemy = aProjectileEnemy.GetComponent<Enemy>();
        aEnemy.speed = mProjectileSpeed;
        if(aEnemy.speed < speed)
        {
            aEnemy.speed = speed;
        }
        Vector3 aMovementVector = mDirection * speed * Time.deltaTime;
        aProjectileEnemy.transform.position = transform.position;
        aEnemy.mSpawnerIndex = mSpawnerIndex;
        aEnemy.mDirection = mDirection;
        aEnemy.type = EnemyType.projectile;
        aEnemy.mPortalDelay = EnemySpawner.Instance.mPortalDelay;
        mSprite.sprite = mWithoutHatSprite;
        AudioManager.Instance.PlaySFX("Hat_Throw");
        aEnemy.SpawnEnemy();
        Bounds aMyBounds = GetComponent<Collider2D>().bounds;
        Bounds aProjBounds = aEnemy.GetComponent<Collider2D>().bounds;
        while(aMyBounds.Intersects(aProjBounds))
        {
            aProjectileEnemy.transform.position += aMovementVector * 2.0f;
            aProjBounds.center += aMovementVector * 2.0f;
        }
        aProjectileEnemy.transform.position += aMovementVector * 5.0f;
    }

}
