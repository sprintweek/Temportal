﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class Enemy : MonoBehaviour,IInitable
{
    public enum State
    {
        None = -1,
        Spawn,
        Move,
        EntryPortal,
        ExitPortal,
        Die
    }
    protected bool mAbilityDone = false;
    [HideInInspector] public float speed;
    Rigidbody2D body;
    protected SpriteRenderer mSprite;
    [HideInInspector] public int mSpawnerIndex;
    [HideInInspector] public EnemyType type;
    [SerializeField] string mPoolName;
    [HideInInspector] public Vector2 mDirection;
    [HideInInspector] public State mState = State.None;
    [HideInInspector] public float mPortalDelay;
    [HideInInspector] public bool mCollided = false;
    public void Init()
    {
        body = GetComponent<Rigidbody2D>();
        mSprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        switch(mState)
        {
            case State.Spawn:
                SpawnState();
                break;
            case State.Move:
                MoveEnemy();
                break;
            case State.EntryPortal:
            case State.ExitPortal:
                PortalState();
                break;
            case State.Die:
                DieState();
                break;
        }
    }


    protected virtual void OnDisable()
    {
        mState = State.None;
    }

    public void SpawnEnemy()
    {
        mCollided = false;
        mState = State.Spawn;
        mAbilityDone = false;
        gameObject.SetActive(true);
    }

    protected virtual void SpawnState()
    {
        mState = State.Move;
    }

    protected virtual void PortalState()
    {
        Color aCol = mSprite.color;
        aCol.a = 0;
        mSprite.color = aCol;
        mPortalDelay -= Time.deltaTime;
        if (mPortalDelay <= 0)
        {
            transform.position = mState == State.EntryPortal ? PortalManager.Instance.mExitPortal.transform.position :
                PortalManager.Instance.mEntryPortal.transform.position;
            AudioManager.Instance.PlaySFX("Warp_Out");
            aCol.a = 1;
            mSprite.color = aCol;
            mDirection = (transform.position - new Vector3(0, 0, 0)).normalized;
            mState = State.Move;
        }
    }

    protected virtual void PushBack()
    {
        if(!LeanTween.isTweening(gameObject))
        {
            LeanTween.move(gameObject, transform.position - new Vector3(mDirection.x, mDirection.y, 0) * EnemySpawner.Instance.mExitPortalPushback, 0.3f)
                .setOnComplete(() => 
                {
                    mState = State.Move;
                });
        }
    }

    protected virtual void DieState()
    {
        ObjectPoolingManager.Instance.ReturnPooledObject(mPoolName, gameObject);
        gameObject.SetActive(false);
    }


    protected virtual void MoveEnemy()
    {
        Vector3 aMovementVector = mDirection * speed * Time.deltaTime;
        transform.position += aMovementVector;
    }

    protected virtual void DoSpecialAbility(Collider2D collision)
    {
        
    }


    public void PlayerHit()
    {
        mState = State.Die;
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        Enemy aEnemy = collision.collider.gameObject.GetComponent<Enemy>();
        if(aEnemy != null)
        {
            mState = State.Die;
            aEnemy.mState = State.Die;
            GameManager.Instance.score++;
            AudioManager.Instance.PlaySFX("ScoreUp", 0.2f);
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<EnemySpawner>())
        {
            mState = State.Die;
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<PortalManager>() && !mAbilityDone)
        {
            DoSpecialAbility(collision);
            mAbilityDone = true;
        }
    }

}
