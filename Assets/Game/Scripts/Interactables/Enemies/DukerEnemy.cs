﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DukerEnemy : Enemy
{
    protected override void DoSpecialAbility(Collider2D collision)
    {
        AudioManager.Instance.PlaySFX("Shot");
        Spawners aNewSpawner = EnemySpawner.Instance.GetAdjacentRandomSpawner(ref mSpawnerIndex);
        mDirection = (aNewSpawner.mSpawnerDirection.position - aNewSpawner.mSpawnerPosition.position).normalized;
        transform.position = GenHelpers.GetClosestPoint(aNewSpawner.mSpawnerPosition.position, aNewSpawner.mSpawnerDirection.position, transform.position);
        Vector3 aMove = mDirection * 2.0f;
        while (collision.bounds.Contains(transform.position))
        {
            transform.position -= aMove;
        }
    }

}
