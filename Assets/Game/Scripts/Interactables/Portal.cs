﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    [SerializeField] bool mEntry;
    [HideInInspector] public PortalIndex mIx = PortalIndex.Top;
    [SerializeField] float mMoveTimer = 0.2f;
    float mCurTimer = 0.2f;
    bool mActive = false;
    [HideInInspector] public bool mInActive = true;
    [SerializeField] ParticleSystem mStartEffect;
    [SerializeField] SpriteRenderer mRenderer;
    [SerializeField] ParticleSystem mBeatEffect;
    void Start()
    {
        mIx = mEntry ? PortalIndex.Top : PortalIndex.Bottom;
        GameManager.Instance.m_GameStart.AddListener(PlayStartParticleSystem);
        DeactivatePortal();
    }

    void PlayStartParticleSystem()
    {
        mStartEffect.Play();
    }

    void OnDestroy()
    {
        if (!GameManager.IsValidSingleton())
        {
            return;
        }
        GameManager.Instance.m_GameStart.RemoveListener(PlayStartParticleSystem);
    }

    void Update()
    {
        if (mActive || mInActive)
        {
            return;
        }
        mCurTimer -= Time.deltaTime;
        if (mCurTimer <= 0.0f)
        {
            mActive = true;
            ActivatePortal();
        }
    }

    public void DeactivatePortal()
    {
        if (mBeatEffect.isPlaying)
        {
            mBeatEffect.Stop();
        }
        mRenderer.color = Color.grey;
    }

    void ActivatePortal()
    {
        mBeatEffect.Play();
        mRenderer.color = Color.white;
    }

    public void MovePortal(Vector3 pPosition)
    {
        mInActive = false;
        transform.position = pPosition;
        mActive = false;
        mCurTimer = mMoveTimer;
        DeactivatePortal();
    }

    void TriggerLogic(Collider2D collision)
    {
        if (!mActive || mInActive)
        {
            return;
        }
        Enemy aEnemy = collision.gameObject.GetComponent<Enemy>();
        if (aEnemy != null)
        {

            if (aEnemy.mCollided)
            {
                return;
            }
            aEnemy.mCollided = true;
            AudioManager.Instance.PlaySFX("Warp_In");
            aEnemy.mState = mEntry ? Enemy.State.EntryPortal : Enemy.State.ExitPortal;
            GameManager.Instance.score++;
            AudioManager.Instance.PlaySFX("ScoreUp", 0.2f);
        }

    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        TriggerLogic(collision);
    }
    void OnTriggerStay2D(Collider2D collision)
    {
        TriggerLogic(collision);
    }

}
