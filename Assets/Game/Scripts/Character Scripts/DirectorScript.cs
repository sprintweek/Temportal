﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectorScript : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy aEnemy = collision.gameObject.GetComponent<Enemy>();
        if(aEnemy != null)
        {
            aEnemy.PlayerHit();
            GameManager.Instance.ReduceHealth(1);
        }
    }
}
