﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : Menu
{
    bool playerOneStart = false;
    bool playerTwoStart = false;
    bool gameStart = false;
    bool tutorial = false;
    [SerializeField]Text[] mHighScore;

    private void OnEnable()
    {
        gameStart = false;
        tutorial = false;
        playerTwoStart = false;
        playerOneStart = false;
        for(int aI = 0; aI < GameManager.Instance.mHighScore.Length; aI ++)
        {
            mHighScore[aI].text = GameManager.Instance.mHighScore[aI].ToString();
        }
        ArcadeInputManager.Instance.mPlayerOneEvents.mSelectPressedEvent.AddListener(PlayerOnePressStart);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mSelectPressedEvent.AddListener(PlayerTwoPressStart);
        ArcadeInputManager.Instance.mPlayerOneEvents.mSwapPressedEvent.AddListener(ShowTutorial);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mSwapPressedEvent.AddListener(ShowTutorial);
        if (!AudioManager.Instance.IsSoundPlaying("MenuMusic"))
        {
            AudioManager.Instance.MusicPlay("MenuMusic", true, 0.1f);
        }
    }

    private void OnDisable()
    {
        if(AudioManager.IsValidSingleton() && AudioManager.Instance.IsSoundPlaying("MenuMusic"))
        {
            AudioManager.Instance.StopSound("MenuMusic");
        }
        if (!ArcadeInputManager.IsValidSingleton())
        {
            return;
        }
        ArcadeInputManager.Instance.mPlayerOneEvents.mSelectPressedEvent.RemoveListener(PlayerOnePressStart);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mSelectPressedEvent.RemoveListener(PlayerTwoPressStart);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mSwapPressedEvent.RemoveListener(ShowTutorial);
        ArcadeInputManager.Instance.mPlayerOneEvents.mSwapPressedEvent.RemoveListener(ShowTutorial);
    }

    void Update()
    {
        if (playerOneStart && playerTwoStart)
        {
            gameStart = true;
            playerTwoStart = false;
            playerOneStart = false;
            GameManager.Instance.LoadGame();
        }
        if (!AudioManager.Instance.IsSoundPlaying("MenuMusic"))
        {
            AudioManager.Instance.MusicPlay("MenuMusic", true, 0.1f);
        }
    }

    void PlayerOnePressStart()
    {
        if(gameStart)
        {
            return;
        }
        playerOneStart = true;
        AudioManager.Instance.PlaySFX("MenuSelectSound");
    }

    void PlayerTwoPressStart()
    {
        if (gameStart)
        {
            return;
        }
        playerTwoStart = true;
        AudioManager.Instance.PlaySFX("MenuSelectSound");
    }

    void ShowTutorial()
    {
        if(gameStart || tutorial)
        {
            return;
        }
        tutorial = true;
        AudioManager.Instance.PlaySFX("MenuSelectSound");
        MenuManager.Instance.ShowMenu(GameManager.Instance.mTutorialMenu);
        MenuManager.Instance.HideMenu(GameManager.Instance.mMainMenu);
    }
}
