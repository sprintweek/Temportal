﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TutorialMenu : Menu
{
    [SerializeField] Image mRenderer;
    [SerializeField] Sprite[] mSpriteArray;
    bool tutorial = false;
    [SerializeField] float mTimeBetweenSprites = 0.5f;
    float mCurrentTimer = 0.0f;
    int mCurIx = 0;
    void OnEnable()
    {
        mRenderer.sprite = mSpriteArray[0];
        mCurIx = -1;
        tutorial = false;
        mCurrentTimer = 0.0f;
        if (!AudioManager.Instance.IsSoundPlaying("MenuMusic"))
        {
            AudioManager.Instance.MusicPlay("MenuMusic", true, 0.1f);
        }
        ArcadeInputManager.Instance.mPlayerTwoEvents.mSwapPressedEvent.AddListener(EndTutorial);
        ArcadeInputManager.Instance.mPlayerOneEvents.mSwapPressedEvent.AddListener(EndTutorial);
    }

    void OnDisable()
    {
        if (AudioManager.IsValidSingleton() && AudioManager.Instance.IsSoundPlaying("MenuMusic"))
        {
            AudioManager.Instance.StopSound("MenuMusic");
        }
        if (!ArcadeInputManager.IsValidSingleton())
        {
            return;
        }
        ArcadeInputManager.Instance.mPlayerTwoEvents.mSwapPressedEvent.RemoveListener(EndTutorial);
        ArcadeInputManager.Instance.mPlayerOneEvents.mSwapPressedEvent.RemoveListener(EndTutorial);
    }

    void Update()
    {
        if(mGroup.alpha <= 0.95f)
        {
            return;
        }
        if (!AudioManager.Instance.IsSoundPlaying("MenuMusic"))
        {
            AudioManager.Instance.MusicPlay("MenuMusic", true, 0.1f);
        }
        mCurrentTimer += Time.deltaTime;
        if(mCurrentTimer >= mTimeBetweenSprites)
        {
            mCurIx = (mCurIx + 1) % mSpriteArray.Length;
            mCurrentTimer = 0.0f;
            mRenderer.sprite = mSpriteArray[mCurIx];
        }
    }

    void EndTutorial()
    {
        if(tutorial)
        {
            return;
        }
        tutorial = true;
        AudioManager.Instance.PlaySFX("MenuSelectSound");
        MenuManager.Instance.ShowMenu(GameManager.Instance.mMainMenu);
        MenuManager.Instance.HideMenu(GameManager.Instance.mTutorialMenu);
    }
}
