﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDMenu : Menu
{
    [SerializeField] Text currentLevel;
    [SerializeField] Text currentScore;
    [SerializeField] Image mHealth;
    [SerializeField] Text mSwitchText;
    void Update()
    {
        float aFillAmnt = GameManager.Instance.health / 3.0f;
        if(aFillAmnt != mHealth.fillAmount)
        {
            mHealth.fillAmount = aFillAmnt;
        }
        currentScore.text = GameManager.Instance.score.ToString();
        currentLevel.text = GameManager.Instance.currentLevel.ToString();
        if (GameManager.Instance.mState == GameManager.State.LevelUp)
        {
            //if(!mSwitchText.gameObject.activeInHierarchy)
            //{
            //    mSwitchText.text = "";
            //    mSwitchText.gameObject.SetActive(true);
            //}
            //if(GameManager.Instance.mLevelUpTimer < 3 && GameManager.Instance.mLevelUpTimer > 2.5)
            //{
            //    mSwitchText.text = "THREE";
            //}
            //else if(GameManager.Instance.mLevelUpTimer < 2.5 && GameManager.Instance.mLevelUpTimer > 1.5)
            //{
            //    mSwitchText.text = "TWO";
            //}
            //else if(GameManager.Instance.mLevelUpTimer < 1.5 && GameManager.Instance.mLevelUpTimer > 0.5)
            //{
            //    mSwitchText.text = "ONE";
            //}
            //else if(GameManager.Instance.mLevelUpTimer < 0.5)
            //{
            //    mSwitchText.text = "SWITCH!!!";
            //}
        }
        else if(mSwitchText.gameObject.activeInHierarchy)
        {
            mSwitchText.gameObject.SetActive(false);
        }
    }


}
