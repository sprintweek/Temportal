﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcadeInputManager : Singleton<ArcadeInputManager>
{
    [HideInInspector] public readonly InputEvents mPlayerOneEvents = new InputEvents();
    [HideInInspector] public readonly InputEvents mPlayerTwoEvents = new InputEvents();

    int mCurPlayerOneHorizontal = -2;
    int mCurPlayerTwoHorizontal = -2;
    int mCurPlayerOneVertical = -2;
    int mCurPlayerTwoVertical = -2;

    void Start()
    {
        mPlayerOneEvents.SetupEvents();
        mPlayerTwoEvents.SetupEvents();
    }


    void Update()
    {
        KeyPressUpdate();
        JoystickCheckUpdate();
    }
    void KeyPressUpdate()
    {
        if(Input.GetButtonDown("OneSubmit"))
        {
            mPlayerOneEvents.mSelectPressedEvent.Invoke();
        }
        if(Input.GetButtonDown("OneCoin"))
        {
            mPlayerOneEvents.mCoinPressedEvent.Invoke();
        }
        if(Input.GetButtonDown("TwoSubmit"))
        {
            mPlayerTwoEvents.mSelectPressedEvent.Invoke();
        }
        if(Input.GetButtonDown("TwoCoin"))
        {
            mPlayerTwoEvents.mCoinPressedEvent.Invoke();
        }
        if(Input.GetButtonDown("OneButton1"))
        {
            mPlayerOneEvents.mSwapPressedEvent.Invoke();
        }
        if(Input.GetButtonDown("TwoButton1"))
        {
            mPlayerTwoEvents.mSwapPressedEvent.Invoke();
        }
    }
    void JoystickCheckUpdate()
    {
        bool aFireP1Event = false;
        bool aFireP2Event = false;
        float aCurP1Hor = Input.GetAxisRaw("OneHorizontal");
        float aCurP1Ver = Input.GetAxisRaw("OneVertical");
        float aCurP2Hor = Input.GetAxisRaw("TwoHorizontal");
        float aCurP2Ver = Input.GetAxisRaw("TwoVertical");

        if (SetAxisValues(ref aCurP1Hor, ref mCurPlayerOneHorizontal))
        {
            aFireP1Event = true;
        }
        if (SetAxisValues(ref aCurP1Ver, ref mCurPlayerOneVertical))
        {
            aFireP1Event = true;
        }
        if (SetAxisValues(ref aCurP2Hor, ref mCurPlayerTwoHorizontal))
        {
            aFireP2Event = true;
        }
        if (SetAxisValues(ref aCurP2Ver, ref mCurPlayerTwoVertical))
        {
            aFireP2Event = true;
        }

        if (aFireP1Event)
        {
            FireJoystickEvents(true);
        }
        if (aFireP2Event)
        {
            FireJoystickEvents(false);
        }
    }


    bool SetAxisValues(ref float pCurVal, ref int pActualVal)
    {
        if (pCurVal >= 1 && pActualVal != 1)
        {
            pActualVal = 1;
            return true;
        }
        else if (pCurVal <= -1 && pActualVal != -1)
        {
            pActualVal = -1;
            return true;
        }
        else if (pCurVal <= 0.01 && pCurVal >= -0.01 && pActualVal != 0)
        {
            pActualVal = 0;
            return true;
        }
        return false;
    }

    void FireJoystickEvents(bool pPlayerOne)
    {
        if(pPlayerOne)
        {
            switch(mCurPlayerOneVertical)
            {
                case -1:
                    {
                        switch(mCurPlayerOneHorizontal)
                        {
                            case -1: mPlayerOneEvents.mBottomLeftEvent.Invoke();
                                break;
                            case 0: mPlayerOneEvents.mBottomEvent.Invoke();
                                break;
                            case 1: mPlayerOneEvents.mBottomRightEvent.Invoke();
                                break;
                        }
                        break;
                    }
                case 0:
                    {
                        switch (mCurPlayerOneHorizontal)
                        {
                            case -1: mPlayerOneEvents.mLeftEvent.Invoke();
                                break;
                            case 0: mPlayerOneEvents.mUntouchedEvent.Invoke();
                                break;
                            case 1: mPlayerOneEvents.mRightEvent.Invoke();
                                break;
                        }
                        break;
                    }
                case 1:
                    {
                        switch (mCurPlayerOneHorizontal)
                        {
                            case -1: mPlayerOneEvents.mTopLeftEvent.Invoke();
                                break;
                            case 0: mPlayerOneEvents.mTopEvent.Invoke();
                                break;
                            case 1: mPlayerOneEvents.mTopRightEvent.Invoke();
                                break;
                        }
                        break;
                    }
            }
        }
        else
        {
            switch (mCurPlayerTwoVertical)
            {
                case -1:
                    {
                        switch (mCurPlayerTwoHorizontal)
                        {
                            case -1:
                                mPlayerTwoEvents.mBottomLeftEvent.Invoke();
                                break;
                            case 0:
                                mPlayerTwoEvents.mBottomEvent.Invoke();
                                break;
                            case 1:
                                mPlayerTwoEvents.mBottomRightEvent.Invoke();
                                break;
                        }
                        break;
                    }
                case 0:
                    {
                        switch (mCurPlayerTwoHorizontal)
                        {
                            case -1:
                                mPlayerTwoEvents.mLeftEvent.Invoke();
                                break;
                            case 0:
                                mPlayerTwoEvents.mUntouchedEvent.Invoke();
                                break;
                            case 1:
                                mPlayerTwoEvents.mRightEvent.Invoke();
                                break;
                        }
                        break;
                    }
                case 1:
                    {
                        switch (mCurPlayerTwoHorizontal)
                        {
                            case -1:
                                mPlayerTwoEvents.mTopLeftEvent.Invoke();
                                break;
                            case 0:
                                mPlayerTwoEvents.mTopEvent.Invoke();
                                break;
                            case 1:
                                mPlayerTwoEvents.mTopRightEvent.Invoke();
                                break;
                        }
                        break;
                    }
            }
        }
    }
}
