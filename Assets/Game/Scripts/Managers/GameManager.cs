﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : Singleton<GameManager>
{
    public enum State
    {
        None = -1,
        Start,
        Gameplay,
        Burst,
        LevelUp,
        Over
    }

    #region Events
    [HideInInspector]public readonly UnityEvent m_GameOver = new UnityEvent();
    [HideInInspector] public readonly UnityEvent m_GameStart = new UnityEvent();
    [HideInInspector] public readonly UnityEvent m_LevelUp = new UnityEvent();
    #endregion
    [HideInInspector] public State mState = State.None;
    [HideInInspector] public int mEnemyCount = 0;
    [HideInInspector]public float mLevelUpTimer;
    public int health = 3;
    [SerializeField] int mEnemiesPerLevel;
    [SerializeField] int mIncreaseEnemiesAfterLevel;
    [HideInInspector] public int currentLevel;
    [HideInInspector] public int score;
    [HideInInspector] public int[] mHighScore = new int[3];
    [SerializeField] float mBurstTimer;
    float mCurrentBurstTimer = 0;
    #region Difficulty Settings
    [Range(0.0f, 1.0f)]
    public float difficultyFactor;

    public int decreaseFactorLevel;
    public int increaseSpeedLevel;
    #endregion

    #region Scene References
    [Header("Game Scenes")]
    [SerializeField] SceneReference mUIScene;
    [SerializeField] SceneReference mGameScene;
    #endregion

    #region Menu Classifiers
    public MenuClassifier mMainMenu;
    public MenuClassifier mHudMenu;
    public MenuClassifier mTutorialMenu;
    #endregion

    void Start()
    {
        MultiSceneManager.Instance.mOnSceneLoad.AddListener(OnSceneLoad);
        MultiSceneManager.Instance.mOnSceneUnload.AddListener(OnSceneUnload);
        MultiSceneManager.Instance.LoadScene(mUIScene);
    }

    void OnDestroy()
    {
        if(!MultiSceneManager.IsValidSingleton())
        {
            return;
        }
        MultiSceneManager.Instance.mOnSceneLoad.RemoveListener(OnSceneLoad);
        MultiSceneManager.Instance.mOnSceneUnload.RemoveListener(OnSceneUnload);
    }

    void Update()
    {
        switch(mState)
        {
            case State.Start:
                {
                    health = 3;
                    score = 0;
                    currentLevel = 1;
                    mCurrentBurstTimer = 0.0f;
                    mEnemyCount = 0;
                    m_GameStart.Invoke();
                    mState = State.Gameplay;
                    break;
                }
            case State.Gameplay:
                {
                    if(!AudioManager.Instance.IsSoundPlaying("TemportalLoop"))
                    {
                        AudioManager.Instance.MusicPlay("TemportalLoop", true, 0.1f);
                        AudioManager.Instance.FadeSound("TemportalLoop", 0.5f, false, 0.1f);
                    }
                    if (mEnemyCount >= mEnemiesPerLevel)
                    {
                        mCurrentBurstTimer = mBurstTimer;
                        mState = State.Burst;
                    }
                    break;
                }
            case State.Burst:
                {
                    if (!AudioManager.Instance.IsSoundPlaying("TemportalLoopBurst"))
                    {
                        AudioManager.Instance.MusicPlay("TemportalLoopBurst", true, 0.1f);
                        AudioManager.Instance.FadeSound("TemportalLoop", 0.5f, true, 0.1f);
                        AudioManager.Instance.FadeSound("TemportalLoopBurst", 0.5f, false, 0.1f);
                    }
                    mCurrentBurstTimer -= Time.deltaTime;
                    if(mCurrentBurstTimer <= 0)
                    {
                        mState = State.LevelUp;
                        mLevelUpTimer = 3.0f;
                    }
                    break;
                }
            case State.LevelUp:
                {
                    if(!AudioManager.Instance.IsSoundPlaying("TemportalLoop"))
                    {
                        AudioManager.Instance.FadeSound("TemportalLoopBurst", 1.5f, true, 0.1f);
                        AudioManager.Instance.MusicPlay("TemportalLoop", true, 0.1f);
                        AudioManager.Instance.FadeSound("TemportalLoop", 1.5f, false, 0.1f);
                    }
                    mLevelUpTimer -= Time.deltaTime;
                    if(mLevelUpTimer <= 0)
                    {
                        currentLevel++;
                        mEnemyCount = 0;
                        mEnemiesPerLevel += mIncreaseEnemiesAfterLevel;
                        if (health < 3)
                        {
                            health++;
                        }
                        if (currentLevel % decreaseFactorLevel == 0)
                        {
                            decreaseFactorLevel *= 2;
                            difficultyFactor += 0.02f;
                            if (difficultyFactor >= 1.0f)
                            {
                                difficultyFactor = 0.95f;
                            }
                        }
                        m_LevelUp.Invoke();
                        mState = State.Gameplay;
                    }
                    break;
                }
            case State.Over:
                {
                    break;
                }
                
        }
    }


    public void ReduceHealth(int pReduction)
    {
        if(health == 0)
        {
            return;
        }
        AudioManager.Instance.PlaySFX("Damage");
        health -= pReduction;
        if (health <= 0)
        {
            health = 0;
            mState = State.Over;
            int aScoreIx = -1;
            for(int aI = 0; aI < mHighScore.Length; aI ++)
            {
                if(score >= mHighScore[aI])
                {
                    aScoreIx = aI;
                    break;
                }
            }
            if(aScoreIx != -1)
            {
                for(int aI = mHighScore.Length - 1; aI > aScoreIx; aI --)
                {
                    mHighScore[aI] = mHighScore[aI - 1];
                }
                mHighScore[aScoreIx] = score;
            }
            m_GameOver.Invoke();
            MenuManager.Instance.HideMenu(mHudMenu);
            MenuManager.Instance.ShowMenu(mMainMenu);
            if (AudioManager.Instance.IsSoundPlaying("TemportalLoop"))
            {
                AudioManager.Instance.FadeSound("TemportalLoop", 0.5f, true, 0.1f);
            }
            if(AudioManager.Instance.IsSoundPlaying("TemportalLoopBurst"))
            {
                AudioManager.Instance.FadeSound("TemportalLoopBurst", 0.5f, true, 0.1f);
            }
            MultiSceneManager.Instance.UnloadScene(mGameScene);
        }
    }


    void OnSceneLoad(List<string> pLoadedScenes)
    {
        if(pLoadedScenes.Contains(mGameScene))
        {
            MenuManager.Instance.ShowMenu(mHudMenu);
            mState = State.Start;
        }
        else
        {

        }
    }

    void OnSceneUnload(List<string> pUnloadedScenes)
    {
        if (pUnloadedScenes.Contains(mGameScene))
        {
            mState = State.None;
        }
        else
        {
            //game end
        }
    }

    public void LoadGame()
    {
        MenuManager.Instance.HideMenu(mMainMenu);
        MultiSceneManager.Instance.LoadScene(mGameScene);
    }


}
