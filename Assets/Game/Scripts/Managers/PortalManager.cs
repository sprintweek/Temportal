﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public enum PortalIndex
{
    Top,
    TopRight,
    Right,
    BottomRight,
    Bottom,
    BottomLeft,
    Left,
    TopLeft,
    None
}

public class PortalManager : Singleton<PortalManager>
{
    [SerializeField] Transform[] mPortalPositions;
    [SerializeField] LayerMask mPortalLayer;
    public Portal mEntryPortal;
    public Portal mExitPortal;

    readonly Dictionary<PortalIndex, UnityAction> mEntryEvents = new Dictionary<PortalIndex, UnityAction>();
    readonly Dictionary<PortalIndex, UnityAction> mExitEvents = new Dictionary<PortalIndex, UnityAction>();

    [HideInInspector]public bool mPlayerOneEntry = true;


    void Start()
    {
        mPlayerOneEntry = false;
        StartEvents();
    }

    void StartEvents()
    {
        CreateEvents();
        RegisterEvents();
        GameManager.Instance.m_LevelUp.AddListener(SwapEvents);
    }

    void CreateEvents()
    {
        foreach(PortalIndex aIx in System.Enum.GetValues(typeof(PortalIndex)))
        {
            if(aIx == PortalIndex.None)
            {
                continue;
            }
            mEntryEvents.Add(aIx, () => MoveActiveEntryPortal(aIx));
            mExitEvents.Add(aIx, () => MoveActiveExitPortal(aIx));
        }
        mEntryEvents.Add(PortalIndex.None, MakeEntryInactive);
        mExitEvents.Add(PortalIndex.None, MakeExitInactive);
    }

    void RegisterEvents()
    {
        if(mPlayerOneEntry)
        {
            SetPlayerOneAsEntry();
            SetPlayerTwoAsExit();
        }
        else
        {
            SetPlayerTwoAsEntry();
            SetPlayerOneAsExit();
        }
    }

    void DeRegisterEvents()
    {
        if (mPlayerOneEntry)
        {
            RemovePlayerOneAsEntry();
            RemovePlayerTwoAsExit();
        }
        else
        {
            RemovePlayerTwoAsEntry();
            RemovePlayerOneAsExit();
        }
    }

    void Update()
    {
        if(mEntryPortal.mIx == mExitPortal.mIx)
        {
            mExitPortal.mIx =(PortalIndex) (((int)mExitPortal.mIx + 1) % (System.Enum.GetValues(typeof(PortalIndex)).Length-1));
            MoveActiveExitPortal(mExitPortal.mIx);
        }
    }

    void OnDestroy()
    {
        if (ArcadeInputManager.IsValidSingleton())
        {
            DeRegisterEvents();
        }
        if (!GameManager.IsValidSingleton())
        {
            return;
        }
        GameManager.Instance.m_LevelUp.RemoveListener(SwapEvents);
    }

    void MakeEntryInactive()
    {
        mEntryPortal.mInActive = true;
        mEntryPortal.DeactivatePortal();
    }

    void MakeExitInactive()
    {
        mExitPortal.mInActive = true;
        mExitPortal.DeactivatePortal();
    }
    void MoveActiveEntryPortal(PortalIndex pIx)
    {
        int aPortalIx = (int)pIx;
        Collider2D aCollider = Physics2D.OverlapPoint(mPortalPositions[aPortalIx].position, mPortalLayer);
        if(aCollider != null)
        {
            if(aCollider.gameObject.GetInstanceID() == mExitPortal.gameObject.GetInstanceID())
            {
                aPortalIx = (aPortalIx + 1) % (System.Enum.GetValues(typeof(PortalIndex)).Length-1);
                MoveActiveEntryPortal((PortalIndex)aPortalIx);
                return;
            }
            else
            {
                mEntryPortal.MovePortal(mPortalPositions[aPortalIx].position);
                mEntryPortal.mIx = (PortalIndex)aPortalIx;
            }
        }
        else
        {
            mEntryPortal.MovePortal(mPortalPositions[aPortalIx].position);
            mEntryPortal.mIx = (PortalIndex)aPortalIx;
        }
    }

    void MoveActiveExitPortal(PortalIndex pIx)
    {
        int aPortalIx = (int)pIx;
        Collider2D aCollider = Physics2D.OverlapPoint(mPortalPositions[aPortalIx].position, mPortalLayer);
        if (aCollider != null)
        {
            if (aCollider.gameObject.GetInstanceID() == mEntryPortal.gameObject.GetInstanceID())
            {
                aPortalIx = (aPortalIx + 1) % (System.Enum.GetValues(typeof(PortalIndex)).Length - 1);
                MoveActiveExitPortal((PortalIndex)aPortalIx);
                return;
            }
            else
            {
                mExitPortal.MovePortal(mPortalPositions[aPortalIx].position);
                mExitPortal.mIx = (PortalIndex)aPortalIx;
            }
        }
        else
        {
            mExitPortal.MovePortal(mPortalPositions[aPortalIx].position);
            mExitPortal.mIx = (PortalIndex)aPortalIx;

        }
    }

    void SwapEvents()
    {
        //DeRegisterEvents();
        //mPlayerOneEntry = !mPlayerOneEntry;
        //RegisterEvents();
        //Vector3 aEntryPos = mExitPortal.transform.position;
        //Vector3 aExitPos = mEntryPortal.transform.position;
        //mEntryPortal.MovePortal(aEntryPos);
        //mExitPortal.MovePortal(aExitPos);
    }
    #region Setting Events
    void SetPlayerOneAsExit()
    {
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopEvent.AddListener(mExitEvents[PortalIndex.Top]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopRightEvent.AddListener(mExitEvents[PortalIndex.TopRight]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mRightEvent.AddListener(mExitEvents[PortalIndex.Right]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomRightEvent.AddListener(mExitEvents[PortalIndex.BottomRight]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomEvent.AddListener(mExitEvents[PortalIndex.Bottom]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomLeftEvent.AddListener(mExitEvents[PortalIndex.BottomLeft]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mLeftEvent.AddListener(mExitEvents[PortalIndex.Left]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopLeftEvent.AddListener(mExitEvents[PortalIndex.TopLeft]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mUntouchedEvent.AddListener(mExitEvents[PortalIndex.None]);
    }

    void SetPlayerTwoAsExit()
    {
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopEvent.AddListener(mExitEvents[PortalIndex.Top]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopRightEvent.AddListener(mExitEvents[PortalIndex.TopRight]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mRightEvent.AddListener(mExitEvents[PortalIndex.Right]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomRightEvent.AddListener(mExitEvents[PortalIndex.BottomRight]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomEvent.AddListener(mExitEvents[PortalIndex.Bottom]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomLeftEvent.AddListener(mExitEvents[PortalIndex.BottomLeft]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mLeftEvent.AddListener(mExitEvents[PortalIndex.Left]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopLeftEvent.AddListener(mExitEvents[PortalIndex.TopLeft]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mUntouchedEvent.AddListener(mExitEvents[PortalIndex.None]);
    }

    void SetPlayerOneAsEntry()
    {
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopEvent.AddListener(mEntryEvents[PortalIndex.Top]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopRightEvent.AddListener(mEntryEvents[PortalIndex.TopRight]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mRightEvent.AddListener(mEntryEvents[PortalIndex.Right]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomRightEvent.AddListener(mEntryEvents[PortalIndex.BottomRight]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomEvent.AddListener(mEntryEvents[PortalIndex.Bottom]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomLeftEvent.AddListener(mEntryEvents[PortalIndex.BottomLeft]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mLeftEvent.AddListener(mEntryEvents[PortalIndex.Left]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopLeftEvent.AddListener(mEntryEvents[PortalIndex.TopLeft]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mUntouchedEvent.AddListener(mEntryEvents[PortalIndex.None]);
    }

    void SetPlayerTwoAsEntry()
    {
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopEvent.AddListener(mEntryEvents[PortalIndex.Top]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopRightEvent.AddListener(mEntryEvents[PortalIndex.TopRight]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mRightEvent.AddListener(mEntryEvents[PortalIndex.Right]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomRightEvent.AddListener(mEntryEvents[PortalIndex.BottomRight]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomEvent.AddListener(mEntryEvents[PortalIndex.Bottom]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomLeftEvent.AddListener(mEntryEvents[PortalIndex.BottomLeft]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mLeftEvent.AddListener(mEntryEvents[PortalIndex.Left]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopLeftEvent.AddListener(mEntryEvents[PortalIndex.TopLeft]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mUntouchedEvent.AddListener(mEntryEvents[PortalIndex.None]);
    }
    #endregion
    #region Removing Events
    void RemovePlayerOneAsExit()
    {
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopEvent.RemoveListener(mExitEvents[PortalIndex.Top]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopRightEvent.RemoveListener(mExitEvents[PortalIndex.TopRight]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mRightEvent.RemoveListener(mExitEvents[PortalIndex.Right]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomRightEvent.RemoveListener(mExitEvents[PortalIndex.BottomRight]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomEvent.RemoveListener(mExitEvents[PortalIndex.Bottom]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomLeftEvent.RemoveListener(mExitEvents[PortalIndex.BottomLeft]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mLeftEvent.RemoveListener(mExitEvents[PortalIndex.Left]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopLeftEvent.RemoveListener(mExitEvents[PortalIndex.TopLeft]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mUntouchedEvent.RemoveListener(mExitEvents[PortalIndex.None]);
    }

    void RemovePlayerTwoAsExit()
    {
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopEvent.RemoveListener(mExitEvents[PortalIndex.Top]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopRightEvent.RemoveListener(mExitEvents[PortalIndex.TopRight]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mRightEvent.RemoveListener(mExitEvents[PortalIndex.Right]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomRightEvent.RemoveListener(mExitEvents[PortalIndex.BottomRight]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomEvent.RemoveListener(mExitEvents[PortalIndex.Bottom]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomLeftEvent.RemoveListener(mExitEvents[PortalIndex.BottomLeft]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mLeftEvent.RemoveListener(mExitEvents[PortalIndex.Left]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopLeftEvent.RemoveListener(mExitEvents[PortalIndex.TopLeft]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mUntouchedEvent.RemoveListener(mExitEvents[PortalIndex.None]);

    }

    void RemovePlayerOneAsEntry()
    {
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopEvent.RemoveListener(mEntryEvents[PortalIndex.Top]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopRightEvent.RemoveListener(mEntryEvents[PortalIndex.TopRight]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mRightEvent.RemoveListener(mEntryEvents[PortalIndex.Right]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomRightEvent.RemoveListener(mEntryEvents[PortalIndex.BottomRight]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomEvent.RemoveListener(mEntryEvents[PortalIndex.Bottom]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mBottomLeftEvent.RemoveListener(mEntryEvents[PortalIndex.BottomLeft]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mLeftEvent.RemoveListener(mEntryEvents[PortalIndex.Left]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mTopLeftEvent.RemoveListener(mEntryEvents[PortalIndex.TopLeft]);
        ArcadeInputManager.Instance.mPlayerOneEvents.mUntouchedEvent.RemoveListener(mEntryEvents[PortalIndex.None]);
    }

    void RemovePlayerTwoAsEntry()
    {
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopEvent.RemoveListener(mEntryEvents[PortalIndex.Top]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopRightEvent.RemoveListener(mEntryEvents[PortalIndex.TopRight]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mRightEvent.RemoveListener(mEntryEvents[PortalIndex.Right]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomRightEvent.RemoveListener(mEntryEvents[PortalIndex.BottomRight]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomEvent.RemoveListener(mEntryEvents[PortalIndex.Bottom]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mBottomLeftEvent.RemoveListener(mEntryEvents[PortalIndex.BottomLeft]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mLeftEvent.RemoveListener(mEntryEvents[PortalIndex.Left]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mTopLeftEvent.RemoveListener(mEntryEvents[PortalIndex.TopLeft]);
        ArcadeInputManager.Instance.mPlayerTwoEvents.mUntouchedEvent.RemoveListener(mEntryEvents[PortalIndex.None]);
    }
    #endregion
}
