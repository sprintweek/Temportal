﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct Spawners
{
    public Transform mSpawnerPosition;
    public Transform mSpawnerDirection;
    public SpriteRenderer mEnemyLight;
}

public class EnemySpawner : Singleton<EnemySpawner>
{
    [Tooltip("Enemy probability and speeds")]
    [SerializeField] List<EnemyData> mEnemies;
    [Tooltip("Don't Edit this as its programmatically edited")]
    [SerializeField] List<float> mEnemyTypeProbability;
    [Tooltip("The spawn positions of the enemies")]
    [SerializeField] Spawners[] mSpawnerPositions;
    [Tooltip("Enemy Spawn Interval")]
    [SerializeField] float mSpawnInterval = 4f;
    [Tooltip("Enemy Spawn Minimum Interval")]
    [SerializeField] float mMinimumSpawnInterval = 0.5f;
    //[Tooltip("Enemy spawning maximum interval")]
    //[SerializeField] float mMaximumInterval = 5;
    //[Tooltip("Enemy spawning minimum interval")]
    //[SerializeField] float mMinimumInterval = 2;
    //[Tooltip("The Minimum Max time of enemies spawning at max difficulty")]
    //[SerializeField] float mMinMaximumInterval = 3;
    //[Tooltip("The Minimum Min time of enemies spawning at max difficulty")]
    //[SerializeField] float mMinMinimumInterval = 1.5f;
    //[Tooltip("The number of levels after which time interval gets updated using difficulty")]
    [SerializeField] int mUpdateIntervalLevel = 3;
    [Tooltip("The delay between the pair of enemies")]
    [SerializeField] float mPairSpawnDelay = 0.5f;
    [Tooltip("The delay between entry and exit portal")]
    public float mPortalDelay = 0.5f;
    [Tooltip("The exit portal pushback of enemies")]
    public float mExitPortalPushback = 2.0f;
    [Tooltip("Enemy Spawn Sound")]
    [SerializeField] string mSpawnSound = "EnemySpawn2";
    float mCurrentTimer = 0.0f;
    bool mFiring = false;
    bool mActive = false;
    void Start()
    {
        GameManager.Instance.m_LevelUp.AddListener(LevelUpEventHandler);
        GameManager.Instance.m_GameStart.AddListener(StartGame);
        GameManager.Instance.m_GameOver.AddListener(EndGame);
    }
    void OnDestroy()
    {
        if(!GameManager.IsValidSingleton())
        {
            return;
        }
        GameManager.Instance.m_LevelUp.RemoveListener(LevelUpEventHandler);
        GameManager.Instance.m_GameStart.RemoveListener(StartGame);
        GameManager.Instance.m_GameOver.RemoveListener(EndGame);
    }

    void StartGame()
    {
        mActive = true;
    }
    void EndGame()
    {
        mActive = false;
    }

    void Update()
    {
        if(!(GameManager.Instance.mState == GameManager.State.Gameplay
            ||
            GameManager.Instance.mState == GameManager.State.Burst))
        {
            return;
        }
        if (!mActive)
        {
            return;
        }
        if (!mFiring)
        {
            mFiring = true;
            mCurrentTimer = GameManager.Instance.mState == GameManager.State.Burst ? mSpawnInterval/2 : mSpawnInterval;
        }
        else
        {
            mCurrentTimer -= Time.deltaTime;
            if (mCurrentTimer <= 0.0f)
            {
                mFiring = false;
                StartCoroutine(FireEnemies());
            }
        }
    }

    IEnumerator FireEnemies()
    {
        GameManager.Instance.mEnemyCount++;
        int aIgnoreVal = Fire();
        yield return new WaitForSeconds(mPairSpawnDelay);
        Fire(aIgnoreVal);
    }


    int Fire(int pIgnoreVal = -1)
    {
        int aType = 0;
        float aProbability = Random.value;
        float aCurProb = 0;
        for (int aI = mEnemyTypeProbability.Count-1; aI >= 0; aI --)
        {
            aCurProb += mEnemyTypeProbability[aI];
            if(aProbability < aCurProb)
            {
                aType = aI;
                break;
            }

        }
        EnemyData aSelectedEnemy = mEnemies[aType];
        int aSpawnPos = GetRandomSpawnerIndex(pIgnoreVal);
        Spawners aSpawnPosition = mSpawnerPositions[aSpawnPos];
        GameObject aEnemy = ObjectPoolingManager.Instance.GetPooledObject(aSelectedEnemy.mPoolName);
        aEnemy.transform.position = aSpawnPosition.mSpawnerPosition.position;
        Enemy aEnemyScript = aEnemy.GetComponent<Enemy>();
        aEnemyScript.mSpawnerIndex = aSpawnPos;
        aEnemyScript.type = aSelectedEnemy.type;
        aEnemyScript.mDirection = (aSpawnPosition.mSpawnerDirection.position - aSpawnPosition.mSpawnerPosition.position).normalized;
        aProbability = Random.value;
        aCurProb = 0;
        aEnemyScript.speed = 10;
        for (int aI = aSelectedEnemy.mSpeedProbability.Length-1; aI >= 0; aI --)
        {
            aCurProb += aSelectedEnemy.mSpeedProbability[aI];
            if(aProbability < aCurProb)
            {
                aEnemyScript.speed = GameManager.Instance.mState == GameManager.State.Burst ? aSelectedEnemy.mMaxSpeed[aI] : Random.Range(aSelectedEnemy.mMinSpeed[aI], aSelectedEnemy.mMaxSpeed[aI]);
                break;
            }
        }
        AudioManager.Instance.PlaySFX(mSpawnSound);
        LeanTween.value(0,1,0.2f).setOnUpdate((float pVal) => ChangeSpriteAlpha(pVal, aSpawnPos)).setOnComplete
            (() => 
        {
            LeanTween.value(1, 0, 0.2f).setOnUpdate((float pVal) => ChangeSpriteAlpha(pVal, aSpawnPos));
        });
        aEnemyScript.mPortalDelay = mPortalDelay;
        aEnemyScript.SpawnEnemy();
        return aSpawnPos;
    }

    void ChangeSpriteAlpha(float pVal, int pIx)
    {
        Color aColor = mSpawnerPositions[pIx].mEnemyLight.color;
        aColor.a = pVal;
        mSpawnerPositions[pIx].mEnemyLight.color = aColor;
        Material aMat = mSpawnerPositions[pIx].mEnemyLight.material;
        aColor = aMat.GetColor("_BaseColor");
        aColor.a = pVal;
        aMat.SetColor("_BaseColor", aColor);
        mSpawnerPositions[pIx].mEnemyLight.material = aMat;
    }


    void LevelUpEventHandler()
    {
        if(mFiring)
        {
            mFiring = false;
        }
        mActive = false;
        List<EnemyData> aUnlockedEnemies = new List<EnemyData>();
        foreach(EnemyData aData in mEnemies)
        {
            if(aData.mEntryLevel <= GameManager.Instance.currentLevel)
            {
                aUnlockedEnemies.Add(aData);
            }
        }
        if(GameManager.Instance.currentLevel % mUpdateIntervalLevel == 0)
        {
            mSpawnInterval /= 2;
            if(mMinimumSpawnInterval >= mSpawnInterval)
            {
                mSpawnInterval = mMinimumSpawnInterval;
            }
            //float aDif = Random.value * GameManager.Instance.difficultyFactor;
            //mMinimumInterval -= aDif;
            //mMaximumInterval -= aDif;
            //if(mMinimumInterval < mMinMinimumInterval)
            //{
            //    mMinimumInterval = mMinMinimumInterval;
            //}
            //if(mMaximumInterval < mMinMaximumInterval)
            //{
            //    mMaximumInterval = mMinMaximumInterval;
            //}
        }
        IncreaseEnemyProbability(aUnlockedEnemies);
        IncreaseSpeedProbability(aUnlockedEnemies);
        mActive = true;
    }

    void IncreaseEnemyProbability(List<EnemyData> pUnlockedEnemies)
    {
        EnemyData aPrevEnemy = pUnlockedEnemies[0];
        for(int aI = 1; aI < pUnlockedEnemies.Count; aI ++)
        {
            if(mEnemyTypeProbability[mEnemies.IndexOf(aPrevEnemy)] <= aPrevEnemy.mMinimumSpawnProbability)
            {
                aPrevEnemy = pUnlockedEnemies[aI];
                continue;
            }
            mEnemyTypeProbability[mEnemies.IndexOf(aPrevEnemy)] *= GameManager.Instance.difficultyFactor;
            int aIx = mEnemies.IndexOf(pUnlockedEnemies[aI]);
            mEnemyTypeProbability[aIx] = 0;
            mEnemyTypeProbability[aIx] = 1 - ProbabilitySum(mEnemyTypeProbability.ToArray());
            aPrevEnemy = pUnlockedEnemies[aI];
        }
    }

    void IncreaseSpeedProbability(List<EnemyData> pUnlockedEnemies)
    {
        for(int aI = 0; aI < pUnlockedEnemies.Count; aI ++)
        {
            for(int aJ = pUnlockedEnemies[aI].mSpeedProbability.Length - 1; aJ >= 0 ; aJ --)
            {
                if (pUnlockedEnemies[aI].mEntryLevel + (aJ + 1) * GameManager.Instance.increaseSpeedLevel > GameManager.Instance.currentLevel)
                {
                    int aEnIx = mEnemies.IndexOf(pUnlockedEnemies[aI]);
                    int aK = 0;
                    while(aK < aJ)
                    {
                        SpeedProbabilityIncreaser(aEnIx, aK, aK + 1);
                        aK++;
                    }
                    break;
                }
            }
        }
    }

    void SpeedProbabilityIncreaser(int pEnemy, int pFrom, int pTo)
    {
        if (mEnemies[pEnemy].mSpeedProbability[pFrom] <= 0.1f)
            return;
        mEnemies[pEnemy].mSpeedProbability[pFrom] *= GameManager.Instance.difficultyFactor;
        mEnemies[pEnemy].mSpeedProbability[pTo] = 0;
        mEnemies[pEnemy].mSpeedProbability[pTo] = 1 - ProbabilitySum(mEnemies[pEnemy].mSpeedProbability);
    }

    float ProbabilitySum(float[] pProbabilities)
    {
        float aSum = 0.0f;
        for (int aI = 0; aI < pProbabilities.Length; aI++)
        {
            aSum += pProbabilities[aI];
        }
        return aSum;
    }



    int GetRandomSpawnerIndex(int pIgnoreVal)
    {
        int aSpawnPos = Random.Range(0, mSpawnerPositions.Length);
        while (aSpawnPos == pIgnoreVal)
        {
            aSpawnPos = Random.Range(0, mSpawnerPositions.Length);
        }
        return aSpawnPos;
    }

    public Spawners GetRandomSpawner(int pIgnoreVal)
    {
        return mSpawnerPositions[GetRandomSpawnerIndex(pIgnoreVal)];
    }

    public Spawners GetAdjacentRandomSpawner(ref int pCurrentSpawner)
    {
        if(Random.value > 0.5f)
        {
            pCurrentSpawner = (pCurrentSpawner + 1) % mSpawnerPositions.Length;
        }
        else
        {
            pCurrentSpawner -= 1;
            if(pCurrentSpawner < 0)
            {
                pCurrentSpawner = mSpawnerPositions.Length - 1;
            }
        }
        return mSpawnerPositions[pCurrentSpawner];
    }

    public Spawners GetNextAdjacent(ref int pCurrent, bool pLeft)
    {
        if(pLeft)
        {
            pCurrent -= 1;
            if (pCurrent < 0)
            {
                pCurrent = mSpawnerPositions.Length - 1;
            }
            pCurrent -= 1;
            if (pCurrent < 0)
            {
                pCurrent = mSpawnerPositions.Length - 1;
            }
        }
        else
        {
            pCurrent = (pCurrent + 2) % mSpawnerPositions.Length;
        }
        return mSpawnerPositions[pCurrent];
    }

}
