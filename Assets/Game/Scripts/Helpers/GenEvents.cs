﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SceneLoadedEvent : UnityEvent<List<string>> { }

[Serializable]
public class InputEvents
{
    public UnityEvent mUntouchedEvent;
    public UnityEvent mTopEvent;
    public UnityEvent mBottomEvent;
    public UnityEvent mLeftEvent;
    public UnityEvent mRightEvent;
    public UnityEvent mTopLeftEvent;
    public UnityEvent mTopRightEvent;
    public UnityEvent mBottomLeftEvent;
    public UnityEvent mBottomRightEvent;
    public UnityEvent mSwapPressedEvent;
    public UnityEvent mSelectPressedEvent;
    public UnityEvent mCoinPressedEvent;
    public void SetupEvents()
    {
        if(mTopEvent != null)
        {
            return;
        }
        mUntouchedEvent = new UnityEvent();
        mTopEvent = new UnityEvent();
        mBottomEvent = new UnityEvent();
        mLeftEvent = new UnityEvent();
        mRightEvent = new UnityEvent();
        mTopLeftEvent = new UnityEvent();
        mTopRightEvent = new UnityEvent();
        mBottomLeftEvent = new UnityEvent();
        mBottomRightEvent = new UnityEvent();
        mSwapPressedEvent = new UnityEvent();
        mSelectPressedEvent = new UnityEvent();
        mCoinPressedEvent = new UnityEvent();
    }

}