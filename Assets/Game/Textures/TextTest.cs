﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextTest : MonoBehaviour
{
    public Text text;
    public float scoreInt;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        scoreInt = 0;
    }

    // Update is called once per frame
    void Update()
    {


        text.text = ("Score Counter = " + scoreInt);


    }
}
